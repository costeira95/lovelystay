import { IUser } from "api/types/User.type";
import { useNavigate } from "react-router-dom";
import styles from "./userInfo.module.scss";
import { useCallback } from "react";

export interface UserInfoProps {
  userData: IUser;
}

export const UserInfo = ({ userData }: UserInfoProps) => {
  const navigate = useNavigate();
  const handleOnClick = useCallback(() => navigate("/"), []);

  return (
    <div className={styles.info}>
      <img className={styles.image} src={userData?.avatar_url} />
      <div>
        <ul className={styles.list}>
          <li>Name: {userData?.name}</li>
          <li>Total number of repositories: {userData?.public_repos}</li>
        </ul>
        <button className={styles.button} onClick={handleOnClick}>Back</button>
      </div>
    </div>
  );
};
