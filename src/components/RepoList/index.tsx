import { IRepo } from 'api/types/Repo.type';
import styles from './repoList.module.scss';

export interface RepoListProps {
  reposData: IRepo[];
}

export const RepoList = ({ reposData }: RepoListProps) => {
  return (
    <div className={styles.container}>
      {reposData?.map((repo, index) => (
        <ul className={styles.list} key={index}>
          <li>Name: {repo.name}</li>
          {repo?.description && <li>Description: {repo?.description}</li>}
        </ul>
      ))}
    </div>
  );
};
