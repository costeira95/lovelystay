import { useEffect, useState } from 'react';

export const useFetchData = <T extends object>(url: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [data, setData] = useState<T>();

  useEffect(() => {
    const fetchDataFromAPI = async () => {
      setIsLoading(true);
      if (url) {
        const res = await fetch(url);

        if ([200, 201, 204].includes(res.status)) {
          setData(await res.json());
        }
      }
      await setIsLoading(false);
    };

    fetchDataFromAPI();
  }, [url]);

  return { data, isLoading };
};
