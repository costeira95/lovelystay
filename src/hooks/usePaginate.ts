import { multiply } from "cypress/types/lodash";
import { useEffect, useState } from "react";

export interface IPaginate<T extends object> {
  array?: T[];
  pageSize: number;
  pageNumber: number;
}

export const usePaginate = <T extends object>({
  array,
  pageSize,
  pageNumber,
}: IPaginate<T>) => {
  const [paginatedData, setPaginatedData] = useState<T[]>([]);
  const [numberOfPages, setNumberOfPages] = useState<number[]>([]);

  useEffect(() => {
    const arraySlice = array?.slice(
      (pageNumber - 1) * pageSize,
      pageNumber * pageSize
    );

    setPaginatedData(arraySlice || []);
    
    let pageNumbers = [];

    for(let i = 1; i <= Math.ceil((array?.length || 1) / pageSize); i++) {
      pageNumbers.push(i);
    }

    setNumberOfPages(pageNumbers);
  }, [array, pageSize, pageNumber]);

  return {paginatedData, numberOfPages};
};
