import { useCallback, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import styles from './searchPage.module.scss';

const SearchPage = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [text, setText] = useState<string>('');

  const handleOnClick = () => navigate(`/user/${text}`);
  const handleKeyDown = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      navigate(`/user/${text}`);
    }
  };

  const isUserNotFound = location.search.includes('userNotFound');

  return (
    <div className={styles.container}>
      <h1>Welcome to my Github user search!</h1>
      <form className={styles.searchBox}>
        <input
          type="text"
          className={styles.searchInput}
          placeholder="Search.."
          onChange={(e) => setText(e.target.value)}
          onKeyDown={handleKeyDown}
          aria-label="input for searching a github user"
        />

        <button className={styles.searchButton} onClick={handleOnClick}>
          <i className={`fas fa-search ${styles.icon}`}></i>
        </button>
      </form>
      {isUserNotFound && <span>User not found</span>}
    </div>
  );
};

export default SearchPage;
