import { IRepo } from 'api/types/Repo.type';
import { IUser } from 'api/types/User.type';
import { RepoList } from 'components/RepoList';
import { Spinner } from 'components/Spinner';
import { UserInfo } from 'components/UserInfo';
import { useFetchData } from 'hooks/useFetchData';
import { usePaginate } from 'hooks/usePaginate';
import { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import styles from './searchResult.module.scss';

const SearchResult = () => {
  const PAGE_SIZE = 5;
  const navigate = useNavigate();
  const [pageNumber, setPageNumber] = useState<number>(1);
  const { user } = useParams();
  const { data: userData, isLoading } = useFetchData<IUser>(
    `https://api.github.com/users/${user}`
  );

  const { data: reposData } = useFetchData<IRepo[]>(userData?.repos_url || '');
  const { paginatedData, numberOfPages } = usePaginate<IRepo>({
    array: reposData,
    pageNumber,
    pageSize: PAGE_SIZE,
  });

  if (isLoading === false && userData === undefined) {
    navigate(`/?userNotFound`);
  }

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <div className={styles.container}>
      {userData && <UserInfo userData={userData} />}
      {paginatedData && <RepoList reposData={paginatedData} />}
      <div className={styles.pagination}>
        {numberOfPages.map((pageNumber: number) => {
          return (
            <span
              key={pageNumber}
              onClick={() => setPageNumber(pageNumber)}
              className={styles.paginationNumber}
            >
              {pageNumber}
            </span>
          );
        })}
      </div>
    </div>
  );
};

export default SearchResult;
