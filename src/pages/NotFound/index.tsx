import NotFoundImage from "assets/images/404.jpg";
import styles from './notFound.module.scss';

const NotFound = () => <img className={styles.image} src={NotFoundImage} />;

export default NotFound;
