# LovelyStay project

This project is a github user search, you can search any user and is going to give you the
profile picture, name, total number of repositories and a list of repositories with
the name and description!




## Installation

Install lovelystay with npm or yarn

```bash
  git clone https://gitlab.com/costeira95/lovelystay.git
  cd lovelystay
  npm i or yarn
  npm start dev or yarn dev
```
   
## Tech Stack

**Client:** React, Html, sass, vitejs

**API:** Github API