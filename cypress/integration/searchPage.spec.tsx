import React from 'react';
import { mount } from '@cypress/react';
import SearchPage from '../../src/pages/SearchPage';
import { beforeEach, describe } from 'mocha';

describe('check if input exists', () => {
  beforeEach(() => {
    mount(<SearchPage />);
  });

  it('renders learn react link', () => {
    cy.get('input').should('have.attr', 'placeholder', 'Search..');
  });
});
