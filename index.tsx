import React, { lazy } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Spinner } from './src/components/Spinner';
import "./index.styles.scss";

const SearchPage = lazy(() => import("./src/pages/SearchPage"));
const SearchResult = lazy(() => import("./src/pages/SearchResult"));
const NotFound = lazy(() => import("./src/pages/NotFound"));

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
    <React.Suspense fallback={<Spinner />}>
      <Routes>
        <Route path='/' element={<SearchPage />} />
        <Route path='/user/:user' element={<SearchResult />} />
        <Route path='*' element={<NotFound />} />
      </Routes>
      </React.Suspense>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
